import React from "react";
import ToDoList from "./components/ToDoList";

import "./styles.css";

function App() {
	return (
		<>
			<div className="container">
				<ToDoList />
			</div>
		</>
	);
}

export default App;
