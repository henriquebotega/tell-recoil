import React, { useEffect } from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { filteredTodoListState } from "../selectors/filteredTodoListState";

import ToDoListStats from "./ToDoListStats";
import ToDoListFilters from "./ToDoListFilters";
import ToDoInput from "./ToDoInput";
import ToDoItem from "./ToDoItem";
import { todoListState } from "../atoms/todoListState";

import useLocalStorage from "../services/useLocalStorage";

import { getRandomArbitrary } from "../services/getRandomArbitrary";

const ToDoList = () => {
	const [persisted, setPersisted] = useLocalStorage("todos", []);

	const todoList = useRecoilValue(filteredTodoListState);
	const setTodoList = useSetRecoilState(todoListState);

	useEffect(() => {
		setPersisted(todoList);
	}, [todoList]);

	useEffect(() => {
		const getCollection = async () => {
			let col = persisted.length > 0 ? persisted : [];

			if (col.length == 0) {
				const response = await fetch(
					"https://jsonplaceholder.typicode.com/todos"
				);

				const body = await response.json();
				col = body.splice(0, 10);
			}

			col.forEach((i) => {
				setTodoList((oldTodoList) => {
					const newItem = [
						...oldTodoList,
						{
							id: getRandomArbitrary(),
							title: i.title,
							isComplete: i.isComplete || false,
						},
					];

					return newItem;
				});
			});
		};

		getCollection();
	}, []);

	return (
		<>
			<ToDoListStats />
			<ToDoListFilters />
			<ToDoInput />

			{todoList.map((todoItem) => (
				<ToDoItem
					key={todoItem.id}
					item={todoItem}
					// setPersisted={setPersisted}
				/>
			))}
		</>
	);
};

export default ToDoList;
