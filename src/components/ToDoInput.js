import React, { useState } from "react";
import { useSetRecoilState } from "recoil";
import { todoListState } from "../atoms/todoListState";
import { getRandomArbitrary } from "../services/getRandomArbitrary";

const ToDoInput = () => {
	const [inputValue, setInputValue] = useState("");
	const setTodoList = useSetRecoilState(todoListState);

	const addItem = () => {
		setTodoList((oldTodoList) => [
			...oldTodoList,
			{
				id: getRandomArbitrary(),
				title: inputValue,
				isComplete: false,
			},
		]);
		setInputValue("");
	};

	const onChange = (e) => {
		setInputValue(e.target.value);
	};

	return (
		<div>
			<input type="text" value={inputValue} onChange={onChange} />
			<button onClick={addItem}>Add</button>
		</div>
	);
};

export default ToDoInput;
